package com.jonasrodrigues;

import com.jonasrodrigues.assets.Asset;
import com.jonasrodrigues.assets.IAsset;

public class Main {
    public static void main(String[] args) {
        System.out.println("Build a Stock!");
        IAsset asset = Asset.BuilderStock.create().quantity(10).value(10.0).build();
        System.out.println("Asset " + asset.getType());
        System.out.println("Tax " + asset.getTax());
        System.out.println("Total " + asset.getQuantity() * asset.getValue());
    }
}