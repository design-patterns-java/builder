package com.jonasrodrigues.assets;

public interface IAsset {

    AssetType getType();

    int getQuantity();

    double getValue();

    double getTax();

}
