package com.jonasrodrigues.assets;

public class Asset implements IAsset {

    private AssetType assetType;
    private int quantity;
    private double value;
    private double tax;

    private Asset() {
    }

    @Override
    public AssetType getType() {
        return assetType;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public double getTax() {
        return tax;
    }

    public static class BuilderStock implements IBuilder<Asset, BuilderStock> {

        private Asset entity;

        private BuilderStock() {
            entity = new Asset();
        }

        public static BuilderStock create() {
            return new BuilderStock();
        }

        public BuilderStock quantity(int quantity) {
            entity.quantity = quantity;
            return this;
        }

        public BuilderStock value(double value) {
            entity.value = value;
            return this;
        }

        @Override
        public Asset build() {
            entity.tax = 0.5;
            entity.assetType = AssetType.STOCK;
            return entity;
        }
    }

}
