package com.jonasrodrigues.assets;

public enum AssetType {
    STOCK, REIT, REAL_STATE
}
