package com.jonasrodrigues.assets;

public interface IBuilder <E, B> {

    E build();
}
