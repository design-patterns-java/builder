# Design Patterns OOP with Java
## _Builder_

This project implements a simplified example for design pattern  Builder

## Example
You make an asset of type stock and multiple other fields:

```java
class Asset {
    private AssetType type;
    private ... 
     ...
}

Asset asset = new Asset();
asset.setAssetType(STOCK);
asset ...
```

With the build pattern, the encapsulation is maintained and the creation process is simplified:

```java
Asset.BuildStock().create()
    .quantity(10)
    .value(5)
    .build();
```
The set of common characteristics of stock is not necessary.
